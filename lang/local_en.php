<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
    // A
    'about' => 'About',

    //
    'by' => 'By',

    // C
    'contents' => 'Contents',
    'contact_info' => 'Contact Info',


    //G
    'gallery' => 'Gallery',
    'gotit' => 'Got it. Thanks!',

    //H
    'home' => 'Home',


    // K

    'keywords' => 'software, oil, gas, thermodynamics, simulatori, consulting, phase equilibria, modeling, equation of state, data crunching, reservoir engineering, pretroleum, gpec',
    'know_us' => 'Meet us',

    // L
    'last_tweet' => 'Último tweet',


    // P

    'photos' => 'Photos',
    'posted' => 'Posted',

    'popular_posts' => 'Popular posts',

    //R
    'readmore' => 'Read more',
    'rubrique' => 'Category',
    'rubriques' => 'Categories',

    //S
    'slogan' => 'Software and consulting in thermodynamics<br>for the oil and chemical industries.',
    'slogan2' => 'Software and consulting in thermodynamics for the oil and chemical industries.',


    //T
    'tags' => 'Tags',
    'team' => 'The team',

    //V
    'view_in' => 'Ver en '
);

?>
