<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
    // A
    'about' => 'Sobre',

    //B
    'by' => 'Por',

    // C
    'contents' => 'Contenidos',
    'contact_info' => 'Información de Contacto',

    //G
    'gallery' => 'Galería de imágenes',
    'gotit' => 'Recibido, muchas gracias!',

    //H
    'home' => 'Inicio',

    // K
    'keywords' => 'software, petróleo, gas, termodinámica, equilibrio de fase,  consultoría, modelado, ecuaciones de estado, visualizacion de datos, gpec, fase global, ingniería de reservorios, simulación',
    'know_us' => 'Conocenos',

    // L
    'last_tweet' => 'Last tweet',

    // P
    'photos' => 'Fotos',
    'posted' => 'Publicado',

    'popular_posts' => 'Artículos destacados',

    //R
    'readmore' => 'Leer más',
    'rubrique' => 'Categoría',
    'rubriques' => 'Categorías',

    //S
    'slogan' => 'Software y consultoría en termodinámica<br>para la industria química y del petróleo',
    'slogan2' => 'Software y consultoría en termodinámica para la industria química y del petróleo',


    //T
    'tags' => 'Palabras clave',
    'team' => 'El equipo',

    //V
    'view_in' => 'View in '
);

?>
