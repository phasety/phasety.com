import os
from deployer.service import Service
from deployer.host import SSHHost, LocalHost


class Phasety(SSHHost):
    slug = 'phasety'
    address = 'phasety.com'
    username = 'phasety'
    password = 'estahablandodelfaso'


class Spip(Service):

    localhost = LocalHost()
    local_squelettes = os.path.abspath(os.path.dirname(__file__))

    root = '/home/phasety/phasety.com'
    squelettes = root + '/squelettes'

    class Hosts:
        host = [Phasety]

    def run(self, *command):
        with self.hosts.cd(self.squelettes):
            self.hosts.run(' '.join(command))

    def deploy(self, branch='master'):
        with self.hosts.cd(self.squelettes):
            self.hosts.run('git checkout %s' % branch)
            self.hosts.run('git pull origin')

if __name__ == '__main__':
    from deployer.run.standalone_shell import start
    start(Spip)
